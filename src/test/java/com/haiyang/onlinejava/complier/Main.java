package com.haiyang.onlinejava.complier;

import java.io.IOException;

public class Main {
    public static void main(String[] args) {
        System.out.println("hello world");
        try {
            try {
                Runtime.getRuntime().exec("notepad");
            } catch (IOException e) {
                e.printStackTrace();
            }
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
